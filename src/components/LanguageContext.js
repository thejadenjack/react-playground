import React, { Component } from 'react';
import { oneOfType, string, object } from 'prop-types';

const { Provider: ContextProvider, Consumer } = React.createContext('en');

class LanguageProvider extends Component {
  state = {
    // eslint-disable-next-line react/destructuring-assignment
    lang: this.props.lang,
  }

  updateLanguage = (lang) => {
    console.log(lang);
    this.setState({ lang });
  }

  render() {
    const { children } = this.props;
    const { lang } = this.state;

    return (
      <ContextProvider value={{ lang, updateLanguage: this.updateLanguage }}>
        {children}
      </ContextProvider>
    );
  }
}

LanguageProvider.propTypes = {
  children: oneOfType([
    string,
    object,
  ]),
  lang: string,
};

LanguageProvider.defaultProps = {
  children: () => {},
  lang: 'en',
};

export {
  LanguageProvider as Provider,
  Consumer,
};
