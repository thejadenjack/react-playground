import React from 'react';
import { render, cleanup, fireEvent } from 'react-testing-library';
import Checkbox from './Checkbox';

afterEach(cleanup);

test('Checkbox\'s label is correct', () => {
  const { getByLabelText } = render((
    <Checkbox>
      Test
    </Checkbox>
  ));

  expect(getByLabelText('Test')).not.toBe(null);
});

test('Checkbox\'s indicator correct on initialization', () => {
  const { getByLabelText, rerender } = render((
    <Checkbox checked={false}>
      Test
    </Checkbox>
  ));

  expect(getByLabelText('Test').checked).toBe(false);

  rerender((
    <Checkbox checked>
      Test
    </Checkbox>
  ));

  expect(getByLabelText('Test').checked).toBe(true);
});

test('Change on uncontrolled checkbox updates input', () => {
  const { getByLabelText } = render((
    <Checkbox>
      Test
    </Checkbox>
  ));

  const checkbox = getByLabelText('Test');
  fireEvent.change(checkbox);

  expect(getByLabelText('Test').checked).toBe(true);
});

test('Change on controlled checkbox calls onChange', () => {
  const spy = jest.fn();
  const { getByLabelText } = render((
    <Checkbox checked={false} onChange={spy}>
      <span>
        Test
      </span>
    </Checkbox>
  ));

  const checkbox = getByLabelText('Test');
  fireEvent.change(checkbox);

  expect(spy).toHaveBeenCalledTimes(1);
});
