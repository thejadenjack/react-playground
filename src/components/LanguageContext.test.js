import React from 'react';
import { render } from 'react-testing-library';
import { Provider, Consumer } from './LanguageContext';

test('Consumer displays default value', () => {
  const tree = (
    <Provider lang="en">
      <Consumer>
        {({ lang }) => (
          <div data-testid="content">
            {lang}
          </div>
        )}
      </Consumer>
    </Provider>
  );

  const { getByTestId } = render(tree);

  expect(getByTestId('content').textContent).toBe('en');
});

test('Consumer displays updated value', () => {
  const tree = (
    <Provider lang="en">
      <Consumer>
        {({ lang, updateLanguage }) => {
          return (
            <div>
              <span data-testid="content">
                {lang}
              </span>
              <button type="button" onClick={() => updateLanguage('es')}>
                change
              </button>
            </div>
          );
        }}
      </Consumer>
    </Provider>
  );

  const { getByTestId, getByText } = render(tree);

  expect(getByTestId('content').textContent).toBe('en');

  getByText('change').click();

  expect(getByTestId('content').textContent).toBe('es');
});
