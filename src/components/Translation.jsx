import React from 'react';
import { Consumer } from './LanguageContext';

function Translation(props) {
  return (
    <Consumer>
      {({ lang }) => props[lang]}
    </Consumer>
  );
}

export default Translation;
