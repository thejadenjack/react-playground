import React, { Component } from 'react';
import {
  oneOfType,
  string,
  bool,
  func,
  object,
} from 'prop-types';

import '@/styles/Checkbox.styl';

class Checkbox extends Component {
  // eslint-disable-next-line react/destructuring-assignment
  isControlled = this.props.checked != null;

  state = {
    checked: false,
  }

  getChecked = () => {
    const { checked: externalChecked } = this.props;
    const { checked: internalChecked } = this.state;

    return this.isControlled ? externalChecked : internalChecked;
  }

  handleChange = (event) => {
    const { onChange } = this.props;
    const { checked } = this.state;

    if (this.isControlled) {
      onChange(event);
    } else {
      this.setState({ checked: !checked });
    }
  }

  render() {
    const {
      className,
      children,
      ...props
    } = this.props;

    return (
      <div className={`checkbox-container ${className}`} {...props}>
        <label htmlFor="checkbox" className="checkbox-label">
          {children}
          <input
            id="checkbox"
            type="checkbox"
            data-testid="checkbox-input"
            checked={this.getChecked()}
            onChange={this.handleChange}
          />
          <span className="checkmark" />
        </label>
      </div>
    );
  }
}

Checkbox.propTypes = {
  className: string,
  children: oneOfType([
    string,
    object,
  ]),
  checked: bool,
  onChange: func,
};

Checkbox.defaultProps = {
  className: '',
  children: '',
  checked: null,
  onChange: null,
};

export default Checkbox;
