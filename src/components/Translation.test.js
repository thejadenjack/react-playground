import React from 'react';
import { cleanup, render } from 'react-testing-library';
import { Provider } from './LanguageContext';
import Translation from './Translation';

afterEach(cleanup);

test('displays default translation', () => {
  const { getByTestId } = render((
    <Provider lang="en">
      <div data-testid="translation">
        <Translation
          en="english"
          es="spanish"
        />
      </div>
    </Provider>
  ));

  expect(getByTestId('translation').textContent).toBe('english');
});
