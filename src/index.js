/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import ReactDOM from 'react-dom';

import Checkbox from './components/Checkbox';

import './styles/index.styl';
import { Provider as LanguageProvider } from './components/LanguageContext';
import Translation from './components/Translation';

ReactDOM.render(
  <div>
    <LanguageProvider lang="es">
      <Checkbox>
        <Translation
          en="Test"
          es="Prueba"
        />
      </Checkbox>
    </LanguageProvider>
  </div>,
  document.getElementById('app'),
);
