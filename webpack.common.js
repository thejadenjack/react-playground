/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const { LoaderOptionsPlugin } = require('webpack');
const poststylus = require('poststylus');


module.exports = {
  entry: path.resolve(__dirname, './src/index.js'),
  output: {
    path: path.resolve(__dirname, './public'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      // Load JSX
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
      // Load Styling
      {
        test: /\.styl$/,
        exclude: /node_modules/,
        use: ['style-loader', 'css-loader', 'stylus-loader'],
      },
      // Load Media
      {
        test: /\.(png|svg|jpg|webm)$/,
        exclude: /node_modules/,
        use: { loader: 'file-loader' },
      },
      // Load Fonts
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        exclude: /node_modules/,
        use: { loader: 'file-loader' },
      },
    ],
  },
  resolve: {
    alias: {
      '@': path.resolve(__dirname, './src'),
    },
    extensions: ['.js', '.jsx'],
  },
  plugins: [
    new LoaderOptionsPlugin({
      options: {
        stylus: {
          use: [poststylus(['autoprefixer'])],
        },
      },
    }),
  ],
};
