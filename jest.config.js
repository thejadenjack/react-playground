const path = require('path');

module.exports = {
  moduleNameMapper: {
    '\\.(png|svg|jpg|webm|woff|woff2|eot|ttf|otf)$': path.resolve(__dirname, './src/assets/__mocks__/fileMock.js'),
    '\\.(styl)$': 'identity-obj-proxy',
    '@/(.*)$': './src/$1',
  },
};
